<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/table.css"/>">
    <title>
        <spring:message code="title.enrolledcourse"/>
    </title>
</head>

<body>
<c:url var="imgsource" value="/public/storage/therap.png"/>
<c:url var="sCourseList" value="/courseList"/>
<c:url var="sEnrolledCourseList" value="/enrolledCourseList"/>
<c:url var="sLogout" value="/logout"/>

<div class="element">

    <div class="sidebar">
        <img src="${imgsource}">

        <a class="anchor" href="${sEnrolledCourseList}">
            <spring:message code="button.enrolledcourse"/>
        </a>

        <a class="anchor" href="${sCourseList}">
            <spring:message code="button.allcourse"/>
        </a>

        <a class="anchor" href="${sLogout}">
            <spring:message code="button.logout"/>
        </a>
    </div>

    <div align="center">

        <div class="fade-out">
            <c:out value="${message}"/>
        </div>

        <table class="content-table">
            <thead>
            <tr>
                <th>
                    <spring:message code="column.enrolledcoursename"/>
                </th>
                <th>
                    <spring:message code="column.options"/>
                </th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${enrolledcourse}" var="course">
                <tr>
                    <td>
                        <c:out value="${course.name}"/>
                    </td>
                    <td>
                        <form:form action="/courseUnenrollment?id=${course.id}&page=1" method="post">
                            <button class="delete-button" type="submit">
                                <spring:message code="button.unenrolled"/>
                            </button>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>

