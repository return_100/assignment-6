<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
    <title>
        <spring:message code="title.addcourse"/>
    </title>
</head>

<body>
<c:url var="imgsource" value="/public/storage/therap.png"/>
<c:url var="sCourseList" value="/courseList"/>
<c:url var="sCourse" value="/course"/>
<c:url var="sTraineeList" value="/traineeList"/>
<c:url var="sTraineeRegistration" value="/traineeRegistration"/>
<c:url var="sLogout" value="/logout"/>


<div class="element">

    <div class="sidebar">
        <img src="${imgsource}">

        <a class="anchor" href="${sCourseList}">
            <spring:message code="button.allcourse"/>
        </a>

        <a class="anchor" href="${sCourse}">
            <spring:message code="button.addcourse"/>
        </a>

        <a class="anchor" href="${sTraineeList}">
            <spring:message code="button.traineelist"/>
        </a>

        <a class="anchor" href="${sTraineeRegistration}">
            <spring:message code="button.traineeregistration"/>
        </a>

        <a class="anchor" href="${sLogout}">
            <spring:message code="button.logout"/>
        </a>
    </div>

    <div class="input-grid">
        <form:form modelAttribute="course" method="post" action="/course">

            <div class="error-text">
                <spring:hasBindErrors name="course">
                    <c:if test="${errors.hasFieldErrors('name')}">
                        <c:out value="*${errors.getFieldError('name').defaultMessage}"/>
                    </c:if>
                </spring:hasBindErrors>
            </div>

            <form:hidden path="id"/>
            <form:input placeholder="Course Name" path="name"/>
            <br>

            <button class="register-button" type="submit">
                <spring:message code="${course.isNew() ? 'button.add' : 'button.update'}"/>
            </button>
        </form:form>
    </div>
</div>
</body>
</html>