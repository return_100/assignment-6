<%@ page import="org.springframework.web.servlet.NoHandlerFoundException" %>
<!DOCTYPE HTML>

<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/design.css"/>">
        <title>
            <spring:message code="title.exception" />
        </title>
    </head>

    <body class="error">
        <%
            if (exception instanceof NoHandlerFoundException) {
                response.getWriter().println("Message: Link Not Found<br><br>");
            } else {
                response.getWriter().println("Message: " + exception.getMessage() + "<br><br>");
            }

            response.getWriter().println("Exception Class: " + exception.getClass() + "<br><br>");
            response.getWriter().println("Please Go Back");
        %>
    </body>

</html>