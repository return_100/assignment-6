<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/design.css"/>">
        <title>
            <spring:message code="title.login" />
        </title>
    </head>

    <body>

        <div class="root">
            <div>
                <img src="<c:url value="/public/storage/therap.png"/>">
            </div>

            <div>
                <form:form modelAttribute="loginUser" method="post" action="/">

                    <div class="error-text">
                        <spring:hasBindErrors name="loginUser">
                            <c:choose>
                                <c:when test="${errors.hasFieldErrors('email')}">
                                    <c:out value="*${errors.getFieldError('email').defaultMessage}"/>
                                </c:when>
                                <c:when test="${errors.hasFieldErrors('password')}">
                                    <c:out value="*${errors.getFieldError('password').defaultMessage}"/>
                                </c:when>
                            </c:choose>
                        </spring:hasBindErrors>
                    </div>

                    <div class="input-container">
                        <div style="color: green">
                            <c:out value="${message}"/>
                        </div>

                        <form:input cssClass="text-field-input" placeholder="Email" path="email"/>
                        <form:password cssClass="text-field-input" placeholder="Paaword" path="password"/>
                    </div>

                    <button type="submit">
                        <spring:message code="button.login" />
                    </button>
                </form:form>
            </div>
        </div>

    </body>
</html>