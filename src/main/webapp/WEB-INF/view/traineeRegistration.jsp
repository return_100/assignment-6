<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
    <title>
        <spring:message code="title.registration"/>
    </title>
</head>

<body>
<c:url var="imgsource" value="/public/storage/therap.png"/>
<c:url var="sCourseList" value="/courseList"/>
<c:url var="sCourse" value="/course"/>
<c:url var="sTraineeList" value="/traineeList"/>
<c:url var="sTraineeRegistration" value="/traineeRegistration"/>
<c:url var="sLogout" value="/logout"/>

<div class="element">

    <div class="sidebar">
        <img src="${imgsource}">

        <a class="anchor" href="${sCourseList}">
            <spring:message code="button.allcourse"/>
        </a>

        <a class="anchor" href="${sCourse}">
            <spring:message code="button.addcourse"/>
        </a>

        <a class="anchor" href="${sTraineeList}">
            <spring:message code="button.traineelist"/>
        </a>

        <a class="anchor" href="${sTraineeRegistration}">
            <spring:message code="button.traineeregistration"/>
        </a>

        <a class="anchor" href="${sLogout}">
            <spring:message code="button.logout"/>
        </a>
    </div>

    <div class="input-grid">

        <form:form modelAttribute="user" method="post" action="/traineeRegistration">
            <div class="error-text">
                <spring:hasBindErrors name="user">
                    <c:choose>
                        <c:when test="${errors.hasFieldErrors('name')}">
                            <c:out value="*${errors.getFieldError('name').defaultMessage}"/>
                        </c:when>
                        <c:when test="${errors.hasFieldErrors('email')}">
                            <c:out value="*${errors.getFieldError('email').defaultMessage}"/>
                        </c:when>
                        <c:when test="${errors.hasFieldErrors('password')}">
                            <c:out value="*${errors.getFieldError('password').defaultMessage}"/>
                        </c:when>
                    </c:choose>
                </spring:hasBindErrors>
            </div>

            <form:hidden path="role" />

            <form:input placeholder="Name" path="name"/>
            <br>
            <form:input placeholder="Email" path="email"/>
            <br>
            <form:password placeholder="Password" path="password"/>
            <br>
            <form:password placeholder="Retype Password" path="repassword"/>
            <br>

            <button class="register-button" type="submit">
                <spring:message code="button.register"/>
            </button>
        </form:form>
    </div>

</div>

</body>
</html>