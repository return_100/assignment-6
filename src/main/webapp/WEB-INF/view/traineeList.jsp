<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/table.css"/>">
    <title>
        <spring:message code="title.traineelist"/>
    </title>
</head>

<body>
<c:url var="imgsource" value="/public/storage/therap.png"/>
<c:url var="sCourseList" value="/courseList"/>
<c:url var="sCourse" value="/course"/>
<c:url var="sTraineeList" value="/traineeList"/>
<c:url var="sTraineeRegistration" value="/traineeRegistration"/>
<c:url var="sLogout" value="/logout"/>

<div class="element">

    <div class="sidebar">
        <img src="${imgsource}">

        <a class="anchor" href="${sCourseList}">
            <spring:message code="button.allcourse"/>
        </a>

        <a class="anchor" href="${sCourse}">
            <spring:message code="button.addcourse"/>
        </a>

        <a class="anchor" href="${sTraineeList}">
            <spring:message code="button.traineelist"/>
        </a>

        <a class="anchor" href="${sTraineeRegistration}">
            <spring:message code="button.traineeregistration"/>
        </a>

        <a class="anchor" href="${sLogout}">
            <spring:message code="button.logout"/>
        </a>
    </div>

    <div align="center">

        <div class="fade-out">
            <c:out value="${message}"/>
        </div>

        <table class="content-table">
            <thead>
            <tr>
                <th>
                    <spring:message code="column.traineename"/>
                </th>
                <th>
                    <spring:message code="column.traineemail"/>
                </th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${traineelist}" var="trainee">
                <tr>
                    <td>
                        <c:out value="${trainee.name}"/>
                    </td>
                    <td>
                        <c:out value="${trainee.email}"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>

