<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/grid.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/public/css/table.css"/>">
    <title>
        <spring:message code="title.allcourse"/>
    </title>
</head>

<body>
<c:url var="imgsource" value="/public/storage/therap.png"/>
<c:url var="sCourseList" value="/courseList"/>
<c:url var="sEnrolledCourseList" value="/enrolledCourseList"/>
<c:url var="sCourse" value="/course"/>
<c:url var="sTraineeList" value="/traineeList"/>
<c:url var="sTraineeRegistration" value="/traineeRegistration"/>
<c:url var="sLogout" value="/logout"/>

<div class="element">

    <div class="sidebar">
        <img src="${imgsource}">

        <c:if test="${isAdmin}">
            <a class="anchor" href="${sCourseList}">
                <spring:message code="button.allcourse"/>
            </a>

            <a class="anchor" href="${sCourse}">
                <spring:message code="button.addcourse"/>
            </a>

            <a class="anchor" href="${sTraineeList}">
                <spring:message code="button.traineelist"/>
            </a>

            <a class="anchor" href="${sTraineeRegistration}">
                <spring:message code="button.traineeregistration"/>
            </a>

            <a class="anchor" href="${sLogout}">
                <spring:message code="button.logout"/>
            </a>
        </c:if>

        <c:if test="${not isAdmin}">
            <a class="anchor" href="${sEnrolledCourseList}">
                <spring:message code="button.enrolledcourse"/>
            </a>

            <a class="anchor" href="${sCourseList}">
                <spring:message code="button.allcourse"/>
            </a>

            <a class="anchor" href="${sLogout}">
                <spring:message code="button.logout"/>
            </a>
        </c:if>
    </div>

    <div align="center">

        <div class="fade-out">
            <c:out value="${message}"/>
        </div>

        <table class="content-table">
            <thead>
            <tr>
                <th>
                    <spring:message code="column.coursename"/>
                </th>
                <c:if test="${isAdmin}">
                    <th>
                        <spring:message code="column.numberofenrolledstudent"/>
                    </th>
                </c:if>
                <th>
                    <spring:message code="column.options"/>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${courseList}" var="course">
                <tr>
                    <td>
                        <c:out value="${course.name}"/>
                    </td>

                    <c:if test="${isAdmin}">
                        <td>
                            <c:out value="${course.traineeSetSize}"/>
                        </td>
                    </c:if>

                    <td>
                        <c:if test="${isAdmin}">
                            <div class="table-button-class">
                                <a href="<c:out value="/enrolledTraineeList?id=${course.id}"/>">
                                    <button class="table-button" type="submit">
                                        <spring:message code="button.show"/>
                                    </button>
                                </a>
                                <a href="<c:out value="/course?id=${course.id}"/>">
                                    <button class="table-button" type="submit">
                                        <spring:message code="button.update"/>
                                    </button>
                                </a>
                                <form:form action="/courseDelete?id=${course.id}" method="post">
                                    <button class="delete-button" type="submit">
                                        <spring:message code="button.delete"/>
                                    </button>
                                </form:form>
                            </div>
                        </c:if>
                        <c:if test="${not isAdmin}">
                            <c:choose>
                                <c:when test="${course.courseEnrolled}">
                                    <form:form action="/courseUnenrollment?id=${course.id}&page=0" method="post">
                                        <button class="delete-button" type="submit">
                                            <spring:message code="button.unenrolled"/>
                                        </button>
                                    </form:form>
                                </c:when>
                                <c:otherwise>
                                    <form:form action="/courseEnrollment?id=${course.id}" method="post">
                                        <button class="enrolled-button" type="submit">
                                            <spring:message code="button.enrolled"/>
                                        </button>
                                    </form:form>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>

