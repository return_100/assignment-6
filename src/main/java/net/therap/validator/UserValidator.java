package net.therap.validator;

import net.therap.model.User;
import net.therap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 13/05/2021
 */
@Service
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        if (Objects.nonNull(userService.getUserByEmail(user.getEmail()))) {
            errors.rejectValue("email", "user.email", EXIST_EMAIL);
        } else if (Objects.isNull(user.getName())) {
            errors.rejectValue("name", "user.name", INVALID_NAME);
        } else if (Objects.isNull(user.getEmail())) {
            errors.rejectValue("email", "user.email", INVALID_EMAIL);
        } else if (Objects.isNull(user.getPassword())) {
            errors.rejectValue("password", "user.password", INVALID_PASSWORD);
        } else if (!user.getPassword().equals(user.getRepassword())) {
            errors.rejectValue("password", "user.password", MISMATCH_PASSWORD);
        }

    }
}
