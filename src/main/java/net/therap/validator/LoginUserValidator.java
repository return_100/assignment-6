package net.therap.validator;

import net.therap.model.LoginUser;
import net.therap.model.User;
import net.therap.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

import static net.therap.util.StringConst.EMAIL_NOT_FOUND;
import static net.therap.util.StringConst.WRONG_PASSWORD;

/**
 * @author al.imran
 * @since 20/05/2021
 */
@Service
public class LoginUserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return LoginUser.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        LoginUser loginUser = (LoginUser) o;

        User registeredUser = userService.getUserByEmail(loginUser.getEmail());

        if (Objects.isNull(registeredUser)) {
            errors.rejectValue("email", "user.email", EMAIL_NOT_FOUND);
        } else if (!registeredUser.getPassword().equals(loginUser.getPassword())) {
            errors.rejectValue("password", "user.password", WRONG_PASSWORD);
        }
    }
}
