package net.therap.validator;

import net.therap.model.Course;
import net.therap.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 12/05/2021
 */
@Service
public class CourseValidator implements Validator {

    @Autowired
    private CourseService courseService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Course.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Course course = (Course) o;

        if (Objects.nonNull(courseService.getCourseByName(course.getName()))) {
            errors.rejectValue("name", "course.name", EXIST_COURSE);
        }

        if (Objects.isNull(course.getName())) {
            errors.rejectValue("name", "course.name", INVALID_COURSE);
        }
    }
}
