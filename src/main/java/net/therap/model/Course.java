package net.therap.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 13/04/2021
 */
@Entity
@Table(name = "course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqCourse", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCourse")
    private int id;

    @Column(name = "name", unique = true, nullable = false)
    @NotNull
    @Size(min = 1, max = 6, message = INVALID_COURSE)
    private String name;

    @ManyToMany(mappedBy = "enrolledCourses", fetch = FetchType.EAGER)
    private Set<User> traineeSet;

    @Transient
    private int traineeSetSize;

    @Transient
    private boolean courseEnrolled;

    public Course() {
        this.traineeSet = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getTraineeSet() {
        return traineeSet;
    }

    public void setTraineeSet(Set<User> traineeSet) {
        this.traineeSet = traineeSet;
    }

    public int getTraineeSetSize() {
        return traineeSet.size();
    }

    public void setTraineeSetSize(int traineeSetSize) {
        this.traineeSetSize = traineeSetSize;
    }

    public boolean isCourseEnrolled() {
        return courseEnrolled;
    }

    public void setCourseEnrolled(boolean courseEnrolled) {
        this.courseEnrolled = courseEnrolled;
    }

    public boolean isNew() {
        return id == 0;
    }

    @PreRemove
    public void removeTrainee() {
        for (User trainee : traineeSet) {
            trainee.getEnrolledCourses().remove(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.nonNull(o) && (o instanceof Course)) {
            return (id == ((Course) o).getId() && name.equals(((Course) o).getName()));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return (id + " " + name);
    }
}
