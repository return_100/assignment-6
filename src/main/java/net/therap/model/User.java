package net.therap.model;

import net.therap.util.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 14/04/2021
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqUser", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqUser")
    private int id;

    @Column(name = "name")
    @NotNull
    @Size(min = 1, max = 30, message = INVALID_NAME)
    @Pattern(regexp = NAME_REGEX, message = INVALID_NAME)
    private String name;

    @Column(name = "email", unique = true, nullable = false)
    @NotNull
    @Size(min = 1, max = 30, message = INVALID_EMAIL)
    @Pattern(regexp = EMAIL_REGEX, message = INVALID_EMAIL)
    private String email;

    @Column(name = "password", nullable = false)
    @NotNull
    @Size(min = 4, max = 30, message = INVALID_PASSWORD)
    private String password;

    @Column(name = "role", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    @Transient
    @Size(min = 4, max = 30)
    private String repassword;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "enrollment",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> enrolledCourses;

    public User() {
        this.enrolledCourses = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public Set<Course> getEnrolledCourses() {
        return enrolledCourses;
    }

    public void setEnrolledCourses(Set<Course> enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }

    public boolean isNew() {
        return id == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.nonNull(o) && (o instanceof User)) {
            return (id == ((User) o).getId()
                    && email.equals(((User) o).getEmail())
                    && password.equals(((User) o).getPassword()));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(email + password);
    }
}
