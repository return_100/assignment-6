package net.therap.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static net.therap.util.StringConst.*;

/**
 * @author al.imran
 * @since 20/05/2021
 */
public class LoginUser {

    @NotNull
    @Size(min = 1, max = 30, message = INVALID_EMAIL)
    @Pattern(regexp = EMAIL_REGEX, message = INVALID_EMAIL)
    private String email;

    @NotNull
    @Size(min = 4, max = 30, message = INVALID_PASSWORD)
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
