package net.therap.dao;

import net.therap.model.Course;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
@Repository
public class CourseDao implements Dao<Course> {

    @PersistenceContext
    private EntityManager em;

    private static final String JPQL_FIND = "FROM Course WHERE name = :name";

    private static final String JPQL_FIND_ALL = "FROM Course";

    @Override
    public Course find(String name) {
        try {
            return em.createQuery(JPQL_FIND, Course.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Course> findAll() {
        return em.createQuery(JPQL_FIND_ALL, Course.class)
                .getResultList();
    }

    @Override
    public Course findById(int id) {
        return em.find(Course.class, id);
    }

    @Override
    @Transactional
    public Course saveOrUpdate(Course course) {
        if (course.isNew()) {
            em.persist(course);
        } else {
            course = em.merge(course);
        }

        return course;
    }

    @Override
    @Transactional
    public void delete(int id) {
        Course course = findById(id);
        em.remove(course);
    }
}
