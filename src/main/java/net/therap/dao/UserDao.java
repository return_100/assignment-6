package net.therap.dao;

import net.therap.model.User;
import net.therap.util.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author al.imran
 * @since 14/04/2021
 */
@Repository
public class UserDao implements Dao<User> {

    @PersistenceContext
    private EntityManager em;

    private static final String JPQL_FIND = "FROM User WHERE email = :email ORDER BY name";

    private static final String JPQL_FIND_BY_ROLE = "FROM User WHERE role = :role ORDER BY name";

    @Override
    public User find(String email) {
        try {
            return em.createQuery(JPQL_FIND, User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public User findById(int id) {
        return em.find(User.class, id);
    }

    @Override
    @Transactional
    public User saveOrUpdate(User user) {
        if (user.isNew()) {
            em.persist(user);
        } else {
            user = em.merge(user);
        }

        return user;
    }

    @Transactional
    public List<User> findByRole(Role role) {
        return em.createQuery(JPQL_FIND_BY_ROLE, User.class)
                .setParameter("role", role)
                .getResultList();
    }
}
