package net.therap.service;

import net.therap.dao.CourseDao;
import net.therap.dao.UserDao;
import net.therap.model.Course;
import net.therap.model.User;
import net.therap.util.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author al.imran
 * @since 15/05/2021
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private CourseDao courseDao;

    public void register(User user) {
        userDao.saveOrUpdate(user);
    }

    public User getUserByEmail(String email) {
        return userDao.find(email);
    }

    public User getUserById(int id) {
        return userDao.findById(id);
    }

    public List<User> getUserByRole(Role role) {
        return userDao.findByRole(role);
    }

    public List<Course> getAllCourse(int traineeId) {
        Set<Course> enrolledCourse = getUserById(traineeId).getEnrolledCourses();
        List<Course> courseList = courseDao.findAll();

        for (Course course : courseList) {
            course.setCourseEnrolled(enrolledCourse.contains(course));
        }

        return courseList;
    }

    public Set<Course> getEnrolledCourse(int userId) {
        return userDao.findById(userId).getEnrolledCourses();
    }

    public void enrolledInACourse(int userId, int courseId) {
        Course course = courseDao.findById(courseId);
        User user = userDao.findById(userId);

        user.getEnrolledCourses().add(course);
        course.getTraineeSet().add(user);

        userDao.saveOrUpdate(user);
        courseDao.saveOrUpdate(course);
    }

    public void unenrolledFromACourse(int userId, int courseId) {
        Course course = courseDao.findById(courseId);
        User user = userDao.findById(userId);

        user.getEnrolledCourses().remove(course);
        course.getTraineeSet().remove(user);

        userDao.saveOrUpdate(user);
        courseDao.saveOrUpdate(course);
    }
}
