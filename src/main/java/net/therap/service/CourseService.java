package net.therap.service;

import net.therap.dao.CourseDao;
import net.therap.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author al.imran
 * @since 09/05/2021
 */
@Service
public class CourseService {

    @Autowired
    private CourseDao courseDao;

    public List<Course> getAllCourse() {
        return courseDao.findAll();
    }

    public void saveOrUpdateCourse(Course course) {
        courseDao.saveOrUpdate(course);
    }

    public Course getCourseByName(String coursename) {
        return courseDao.find(coursename);
    }

    public Course getCourseById(int id) {
        return courseDao.findById(id);
    }

    public void deleteCourse(int id) {
        courseDao.delete(id);
    }
}
