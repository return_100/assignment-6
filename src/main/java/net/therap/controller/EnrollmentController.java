package net.therap.controller;

import net.therap.service.UserService;
import net.therap.util.AccessChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author al.imran
 * @since 19/05/2021
 */
@Controller
public class EnrollmentController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AccessChecker accessChecker;

    @PostMapping(value = "/courseEnrollment")
    public String processEnrollment(@RequestParam("id") int courseId,
                                    HttpSession httpSession,
                                    RedirectAttributes redirectAttributes) {

        if (accessChecker.isTrainee(httpSession)) {
            userService.enrolledInACourse((Integer) httpSession.getAttribute("id"), courseId);
            redirectAttributes.addFlashAttribute("message", messageSource.getMessage("response.courseEnrollment", null, null));
        }

        return "redirect:/courseList";
    }

    @PostMapping(value = "/courseUnenrollment")
    public String processUnenrollment(@RequestParam("id") int courseId,
                                      @RequestParam(value = "page", defaultValue = "0") int pageId,
                                      HttpSession httpSession,
                                      RedirectAttributes redirectAttributes) {

        if (accessChecker.isTrainee(httpSession)) {
            userService.unenrolledFromACourse((Integer) httpSession.getAttribute("id"), courseId);
            redirectAttributes.addFlashAttribute("message", messageSource.getMessage("response.courseUnenrollment", null, null));
        }

        return pageId == 0 ? "redirect:/courseList" : "redirect:/enrolledCourseList";
    }
}
