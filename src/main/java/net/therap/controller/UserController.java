package net.therap.controller;

import net.therap.model.User;
import net.therap.service.CourseService;
import net.therap.service.UserService;
import net.therap.util.AccessChecker;
import net.therap.util.Role;
import net.therap.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author al.imran
 * @since 12/05/2021
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private AccessChecker accessChecker;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.addValidators(userValidator);
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/traineeList")
    public String show(ModelMap modelMap,
                       HttpSession httpSession) {

        if (accessChecker.isAdmin(httpSession)) {
            modelMap.addAttribute("traineelist", userService.getUserByRole(Role.Trainee));
            return "traineeList";
        }

        return "redirect:/courseList";
    }

    @GetMapping(value = "/enrolledCourseList")
    public String showEnrolledCourse(HttpSession httpSession,
                                     ModelMap modelMap) {

        if (accessChecker.isTrainee(httpSession)) {
            int id = (Integer) httpSession.getAttribute("id");
            modelMap.addAttribute("enrolledcourse", userService.getEnrolledCourse(id));
            return "enrolledCourseList";
        }

        return "redirect:/courseList";
    }

    @GetMapping(value = "/enrolledTraineeList")
    public String showEnrolledTraineeList(@RequestParam("id") int courseId,
                                          HttpSession httpSession,
                                          ModelMap modelMap) {

        if (accessChecker.isAdmin(httpSession)) {
            modelMap.addAttribute("trainees", courseService.getCourseById(courseId).getTraineeSet());
            return "enrolledTraineeList";
        }

        return "redirect:/courseList";
    }

    @GetMapping(value = "/traineeRegistration")
    public String showTraineeRegistration(HttpSession httpSession,
                                          ModelMap modelMap) {

        if (accessChecker.isAdmin(httpSession)) {
            User user = new User();
            user.setRole(Role.Trainee);

            modelMap.addAttribute("user", user);
            return "traineeRegistration";
        }

        return "redirect:/courseList";
    }

    @PostMapping(value = "/traineeRegistration")
    public String processRegistration(@Valid @ModelAttribute("user") User user,
                                      BindingResult bindingResult,
                                      HttpSession httpSession,
                                      SessionStatus sessionStatus,
                                      RedirectAttributes redirectAttributes) {

        if (accessChecker.isAdmin(httpSession)) {
            if (bindingResult.hasErrors()) {
                return "traineeRegistration";
            }

            userService.register(user);
            redirectAttributes.addFlashAttribute("message", messageSource.getMessage("response.traineeRegister", null, null));
            sessionStatus.setComplete();
            return "redirect:/traineeList";
        }

        return "redirect:/courseList";
    }
}
