package net.therap.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author al.imran
 * @since 11/05/2021
 */
@ControllerAdvice
@EnableWebMvc
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public String process() {
        return "exception";
    }
}
