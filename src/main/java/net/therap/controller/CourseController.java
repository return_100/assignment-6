package net.therap.controller;

import net.therap.model.Course;
import net.therap.service.CourseService;
import net.therap.service.UserService;
import net.therap.util.AccessChecker;
import net.therap.validator.CourseValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author al.imran
 * @since 12/05/2021
 */
@Controller
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private CourseValidator courseValidator;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AccessChecker accessChecker;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.addValidators(courseValidator);
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping(value = "/courseList")
    public String show(ModelMap modelMap,
                       HttpSession httpSession) {

        if (accessChecker.isAdmin(httpSession)) {
            modelMap.addAttribute("courseList", courseService.getAllCourse());
            modelMap.addAttribute("isAdmin", true);
        } else {
            modelMap.addAttribute("courseList", userService.getAllCourse((Integer) httpSession.getAttribute("id")));
            modelMap.addAttribute("isAdmin", false);
        }

        return "courseList";
    }

    @GetMapping(value = "/course")
    public String show(@RequestParam(defaultValue = "0") int id,
                       ModelMap modelMap,
                       HttpSession httpSession) {

        if (accessChecker.isAdmin(httpSession)) {
            modelMap.addAttribute("course", id == 0 ? new Course() : courseService.getCourseById(id));
            return "course";
        }

        return "redirect:/courseList";
    }

    @PostMapping(value = "/course")
    public String process(@Valid @ModelAttribute("course") Course course,
                          BindingResult bindingResult,
                          ModelMap modelMap,
                          HttpSession httpSession,
                          SessionStatus sessionStatus,
                          RedirectAttributes redirectAttributes) {

        if (accessChecker.isAdmin(httpSession)) {
            if (bindingResult.hasErrors()) {
                modelMap.addAttribute("course", course);
                return "course";
            }

            redirectAttributes.addFlashAttribute("message", course.isNew()
                    ? messageSource.getMessage("response.courseAdd", null, null)
                    : messageSource.getMessage("response.courseUpdate", null, null));

            courseService.saveOrUpdateCourse(course);
        }

        sessionStatus.setComplete();
        return "redirect:/courseList";
    }

    @PostMapping(value = "/courseDelete")
    public String processDelete(@RequestParam("id") int courseId,
                                HttpSession httpSession,
                                RedirectAttributes redirectAttributes) {

        if (accessChecker.isAdmin(httpSession)) {
            courseService.deleteCourse(courseId);
            redirectAttributes.addFlashAttribute("message",
                    messageSource.getMessage("response.courseDelete", null, null));
        }

        return "redirect:/courseList";
    }
}
