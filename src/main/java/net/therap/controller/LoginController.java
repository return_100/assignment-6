package net.therap.controller;

import net.therap.model.LoginUser;
import net.therap.model.User;
import net.therap.service.UserService;
import net.therap.util.AccessChecker;
import net.therap.util.Role;
import net.therap.validator.LoginUserValidator;
import net.therap.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author al.imran
 * @since 08/05/2021
 */
@Controller
public class LoginController {

    @Autowired
    private LoginUserValidator loginUserValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AccessChecker accessChecker;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.addValidators(loginUserValidator);
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping(value = "/")
    public String handleGet(ModelMap modelMap) {
        modelMap.addAttribute("loginUser", new LoginUser());
        return "login";
    }

    @PostMapping(value = "/")
    public String handlePost(@Valid @ModelAttribute("loginUser") LoginUser loginUser,
                             BindingResult bindingResult,
                             HttpSession httpSession,
                             RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "login";
        }

        User user = userService.getUserByEmail(loginUser.getEmail());
        httpSession.setAttribute("id", user.getId());
        redirectAttributes.addFlashAttribute("messsage", messageSource.getMessage("response.login", null, null));

        if (accessChecker.isAdmin(httpSession)) {
            httpSession.setAttribute("role", Role.Admin);
            return "redirect:/courseList";
        } else {
            httpSession.setAttribute("role", Role.Trainee);
            return "redirect:/enrolledCourseList";
        }
    }
}
