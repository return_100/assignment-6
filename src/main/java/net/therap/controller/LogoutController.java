package net.therap.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @author al.imran
 * @since 09/05/2021
 */
@Controller
public class LogoutController {

    @Autowired
    private MessageSource messageSource;

    @GetMapping(value = "/logout")
    public String handleGet(HttpSession httpSession,
                            RedirectAttributes redirectAttributes) {

        httpSession.removeAttribute("id");
        httpSession.removeAttribute("role");
        redirectAttributes.addFlashAttribute("message", messageSource.getMessage("response.logout", null, null));
        return "redirect:/";
    }
}
