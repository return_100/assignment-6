package net.therap.util;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

/**
 * @author al.imran
 * @since 19/05/2021
 */
@Service
public class AccessChecker {

    public boolean isAdmin(HttpSession httpSession) {
        return Role.Admin.equals(httpSession.getAttribute("role"));
    }

    public boolean isTrainee(HttpSession httpSession) {
        return Role.Trainee.equals(httpSession.getAttribute("role"));
    }
}
