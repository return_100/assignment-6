package net.therap.util;

/**
 * @author al.imran
 * @since 18/05/2021
 */
public enum Role {
    Admin,
    Trainee
}
