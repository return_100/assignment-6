package net.therap.util;

/**
 * @author al.imran
 * @since 09/05/2021
 */
public interface StringConst {

    String INVALID_COURSE = "Invalid Course Name Format or Length";
    String EXIST_COURSE = "Course Already Exist";
    String NONEXIST_COURSE = "Course Doesn't Exist";
    String UNENROLLED_COURSE = "Course isn't Enrolled";

    String INVALID_NAME = "Invalid Name Format or Length";
    String NAME_REGEX = "[a-zA-Z]+[a-zA-Z0-9]*";

    String EMAIL_NOT_FOUND = "Email Not Found";
    String EMAIL_REGEX = "[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*";
    String INVALID_EMAIL = "Invalid Email Format or Length";
    String EXIST_EMAIL = "Email Already Exist";

    String INVALID_PASSWORD = "Invalid Password Format or Length";
    String MISMATCH_PASSWORD = "Password mismatched";
    String WRONG_PASSWORD = "Wrong Password";
}
