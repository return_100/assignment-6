package net.therap.config;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author al.imran
 * @since 11/05/2021
 */
@Component
@Order(1)
public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

        boolean isInitialLandingPath = req.getRequestURI().equals("/");
        boolean isPublicPath = req.getRequestURI().startsWith("/public");
        boolean isFavicon = req.getRequestURI().startsWith("/favicon");
        boolean isAuthenticated = Objects.nonNull(req.getSession().getAttribute("id")) &&
                Objects.nonNull(req.getSession().getAttribute("role"));

        if (isInitialLandingPath || isPublicPath || isFavicon || isAuthenticated) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            resp.sendRedirect("/");
        }
    }

    @Override
    public void destroy() {

    }
}
